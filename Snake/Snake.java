/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static java.lang.Math.pow;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Krzysiek
 */
public class Snake implements KeyListener{
    ArrayList<Square> squareList;
    int x, y;
    int snakeSquareSize;
    int snakeSize, snakeSpeed;
    boolean isKeyPressed;
    boolean isEating;
    Direction dir;
    
    public Snake(int x, int y){
        isKeyPressed = false;
        isEating = false;
        this.x = x;
        this.y = y;
        snakeSquareSize = 50;
        snakeSize = 2;
        snakeSpeed = 1000;
        dir = Direction.RIGHT;
        squareList = new ArrayList<>();
        squareList.add(new Square(x, y,
                snakeSquareSize, snakeSquareSize, Color.RED));
        squareList.add(new Square(x-snakeSquareSize, y,
                snakeSquareSize, snakeSquareSize, Color.GREEN));
    }
    
    public void move(){
        switch(dir){
            case UP:
                y-=50;
                break;
            case DOWN:
                y+=50;
                break;
            case LEFT:
                x-=50;
                break;
            case RIGHT:
                x+=50;
                break;
        }
        int xToSet = x;
        int yToSet = y;
        for(Square sq: squareList){
           int lx = sq.getX();
           int ly = sq.getY();
           sq.setX(xToSet);
           sq.setY(yToSet);
           xToSet = lx;
           yToSet = ly;
        }
    }
    
    public void addSegment(){
        snakeSize++;
        isEating = true;
        squareList.add(1, new Square(squareList.get(1).getX(), squareList.get(1).getY(),
            snakeSquareSize, snakeSquareSize, Color.BLACK));
    }
    
    public void checkHeadColisionWithPoint(Point point){
        Square headSquare = squareList.get(0);
        if(Square.isSamePosition(headSquare.getX(), headSquare.getY(),
                point.getX(), point.getY())){
            point.setTaken();
        }
    }
    
     public boolean isSelfColision(){
       Square headSquare = squareList.get(0);
       for(int i = 1; i<squareList.size(); i++){
           if(Square.isSamePosition(headSquare.getX(), headSquare.getY(), 
                                    squareList.get(i).getX(),
                                    squareList.get(i).getY())){
               return true;
           }
       }
       return false;
    }
    
    public boolean isColisionWithBoundaries(int x, int y, int offset){
        Square headSquare = squareList.get(0);
        if(headSquare.getX()<offset || headSquare.getX() >= (x+offset) ||
           headSquare.getY()<offset || headSquare.getY() >= (y+offset)){
            return true;
        }
        return false;
    }
     
    public void eat(){
        if(isEating){
                boolean isBlack = false;
                for(int i = 0; i<squareList.size(); i++){
                    if(squareList.get(i).getColor() == Color.BLACK){
                        if(i == squareList.size()-1){
                            squareList.get(i).setColor(Color.GREEN);
                        }
                        else{
                            isBlack = true;
                            squareList.remove(i+1);
                            squareList.add(i, new Square(squareList.get(i).getX(), squareList.get(i).getY(),
            snakeSquareSize, snakeSquareSize, Color.GREEN));
                            i+=1;
                        }      
                    }
                }
                if(!isBlack) isEating = false;
            }   
    }  
    
    public void paintSnake(Graphics2D g){
       for(Square sq: squareList){
           sq.paintSquare(g);
       } 
    }

    public void setIsKeyPressed(){
        isKeyPressed = false;
    }
    
    public ArrayList<Square> getSquareList(){
        return squareList;
    }
    
    public int getSpeed(){
        return snakeSpeed;
    }
    
    public void accelerate(){
        snakeSpeed-=100; 
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
 
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_DOWN){
            if(!isKeyPressed && dir != Direction.UP){
                dir = Direction.DOWN;
                isKeyPressed = true;               
            }   
        }
        if(e.getKeyCode() == KeyEvent.VK_UP){
           if(!isKeyPressed && dir != Direction.DOWN){
                dir = Direction.UP;
                isKeyPressed = true;               
            }
        }
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            if(!isKeyPressed && dir != Direction.RIGHT){
                dir = Direction.LEFT;
                isKeyPressed = true;
            }
        }
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
           if(!isKeyPressed && dir != Direction.LEFT){
                dir = Direction.RIGHT;
                isKeyPressed = true;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
   
}
