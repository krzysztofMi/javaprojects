/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import javax.swing.JFrame;


public class Frame extends JFrame{
    public Frame(){
        this.setTitle("Snake Game");
        Panel panel = new Panel();
        this.add(panel); 
        this.addKeyListener(panel.getSnake());
        this.setVisible(true);
        this.setSize(675, 675);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
    }
}
