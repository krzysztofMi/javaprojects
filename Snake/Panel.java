/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Krzysiek
 */
public class Panel extends JPanel implements ActionListener{
    
    int points;
    int x = 550;
    int y = 550;
    int offset = 50;
    int cellSize = 50;
    Snake snake;
    Point point;
    Timer t;
    public Panel(){
        this.setSize(x, y);
        snake = new Snake(100, 50);
        generateNewPoint();
        t = new Timer(snake.getSpeed(), this);
        t.start();
        
        points = 0;
    }
    
    private void generateNewPoint(){
        do{
            int i = Util.getRandomNumberInts(0, 10);
            int j = Util.getRandomNumberInts(0, 10);
            point = new Point(offset + i*cellSize, offset + j*cellSize);
        }
        while(point.checkColisionWith(snake));
    }
    
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(Color.WHITE);
        g.fillRect(offset, offset, x, y);
        snake.paintSnake((Graphics2D) g);
        point.paintSquare((Graphics2D) g);
    }
    
     public Snake getSnake(){
        return snake;
    }
        
    public void checkPoint(){
        if(point.getTaken()){
            points++;
            if(points%3 == 0 && snake.getSpeed()>100){
                snake.accelerate();
                t.setDelay(snake.getSpeed());
            }
            generateNewPoint();
            snake.addSegment();
            point.setNotTaken();
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        snake.move();
        snake.setIsKeyPressed();
        snake.checkHeadColisionWithPoint(point);
        checkPoint();
        snake.eat();
        endGame();
        this.repaint();
    }
    
    private void endGame(){
        if(snake.isSelfColision() || snake.isColisionWithBoundaries(x, y, offset)){
            JOptionPane.showMessageDialog(null,"Ilosc punktow: "+points ,"", JOptionPane.ERROR_MESSAGE);    
            System.exit(0);
        }
    }
}
