/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.util.ArrayList;


/**
 *
 * @author Krzysiek
 */
public class Point extends Square{
    int value;
    boolean taken;
    public Point(int x, int y){
        super(x, y, 50, 50, Color.ORANGE);
        value = 1;
        taken = false;
    }
    public int getValue(){
        return value;
    }
   
    public void setTaken(){
        taken = true;
    }
    public void setNotTaken(){
        taken = false;
    }
    public boolean getTaken(){
        return taken;
    }
    
    public boolean checkColisionWith(Snake snake){
        ArrayList<Square> squareList = snake.getSquareList();
        for(Square sqr: squareList){
            if(Square.isSamePosition(sqr.getX(), sqr.getY(),
                             x, y)){
                return true;
            }
        }
        return false;
    }
}
