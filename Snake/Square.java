/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author Krzysiek
 */
public class Square {
    int x, y, width, height;
    Color color;
    public Square(int x, int y, int width, int height, Color color){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
    }

    Square(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void paintSquare(Graphics2D g){
        g.setColor(new Color(0,0,0));
        //g.drawRect(x, y, width, height);
        g.setColor(color);
        g.fillRect(x, y, width, height);
    }
    
    public void setColor(Color color){
        this.color = color;
    }
    
    public int getX(){
       return x; 
    }
    public int getY(){
        return y;
    }
    public void setX(int x){
        this.x = x;
    }
     public void setY(int y){
        this.y = y;
    }
    
     public Color getColor(){
         return color;
     }
     
    public static boolean isSamePosition(int x1, int y1, int x2, int y2){
        return x1 == x2 && y1 == y2;
    }
}
